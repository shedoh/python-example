'''
Created on 2014/12/10

@author: Shedoh
'''
import unittest
from jenkins.cal.calculator import Calculator

class Test(unittest.TestCase):
	def setUp(self):
		self.cal = Calculator()

	def tearDown(self):
		pass

	def test_add1(self):
		self.assertEquals(3, self.cal.add(3))

	def test_add2(self):
		self.assertEquals(3, self.cal.add(3))

	def test_multiply(self):
		self.assertEquals(0, self.cal.multiply(3))

if __name__ == "__main__":
	#import sys;sys.argv = ['', 'Test.testName']
	unittest.main()