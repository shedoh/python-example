'''
Created on 2014/12/10

@author: Shedoh
'''

class Calculator(object):
	def __init__(self):
		self.number = 0

	def add(self, i):
		self.number += i
		return self.number

	def multiply(self, i):
		self.number *= i
		return self.number

	def reset(self):
		self.number = 0
		return self.number
